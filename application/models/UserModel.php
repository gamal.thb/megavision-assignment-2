<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getUserByEmail($email) {
        $query = $this->db->get_where('users', array('email' => $email));
        return $query->row_array();
    }

    public function getEmployeeById($id) {
        $query = $this->db->get_where('employees', array('id' => $id));
        return $query->row_array();
    }

}
