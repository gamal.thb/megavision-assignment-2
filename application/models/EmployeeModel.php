<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAllEmployees() {
        return $this->db->get('employees')->result_array();
    }

    public function getEmployeeById($id) {
        $query = $this->db->get_where('employees', array('id' => $id));
        return $query->row_array();
    }

}
