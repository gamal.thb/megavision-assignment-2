
			<!-- Begin Page Content -->
			<div class="container-fluid">

				<!-- Page Heading -->
				<h1 class="h3 mb-2 text-gray-800">Dashboard</h1>

				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<!-- <label for="start_date">Start Date:</label> -->
							<div class="input-group date datepicker" id="start_date_picker" data-target-input="nearest">
								<input type="text" id="start_date" name="start_date" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#start_date_picker" />
								<div class="input-group-append" data-target="#start_date_picker" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<!-- <label for="end_date">End Date:</label> -->
							<div class="input-group date datepicker" id="end_date_picker" data-target-input="nearest">
								<input type="text" id="end_date" name="end_date" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#end_date_picker" />
								<div class="input-group-append" data-target="#end_date_picker" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label>&nbsp;</label>
							<div class="btn-group">
								<button type="button" class="btn btn-primary" id="apply_filter">Apply Filter</button>
								<button type="button" class="btn btn-secondary" id="clear_filter">Clear Filter</button>
							</div>
						</div>
					</div>
				</div>

				<div class="card shadow mb-4">
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Customer Information</h6>
					</div>
					<div class="card-body">
						<!-- <div class="table-responsive"> -->
							<table id="employees_tb" class="table table-bordered nowrap" style="width:100%" cellspacing="0">
								<thead>
									<tr>
										<th>No</th>
										<th>Employee ID</th>
										<th>Employee Name</th>
										<th>Employee Email</th>
										<th>Employee Phone</th>
										<th>Office</th>
										<th>Order Date</th>
										<th>Order Item</th>
										<th>Order Amount</th>
										<th>Client ID</th>
										<th>Client Name</th>
										<th>Client Email</th>
										<th>Client Phone</th>
									</tr>
								</thead>
							</table>
						<!-- </div> -->
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- End of Main Content -->

		<!-- Footer -->
		<footer class="sticky-footer bg-white">
			<div class="container my-auto">
				<div class="copyright text-center my-auto">
					<span>Copyright &copy; Your Website 2020</span>
				</div>
			</div>
		</footer>
		<!-- End of Footer -->

	</div>
	<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a class="btn btn-primary" href="login.html">Logout</a>
			</div>
		</div>
	</div>
</div>