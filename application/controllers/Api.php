<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    public function __construct() {
        parent::__construct();
        $this->load->model('EmployeeModel');
        $this->load->model('UserModel');
        $this->load->library('session');
    }

    public function employees_get() {
        if (!$this->isLoggedIn()) {
            $this->response(['error' => 'Unauthorized! Please Login to proceed.'], 401);
            return;
        }
        $id = $this->get( 'id' );
        if ( $id === null ){
            $employees = $this->EmployeeModel->getAllEmployees();
            $this->response($employees, 200);
        }else {
            $employee = $this->EmployeeModel->getEmployeeById($id);
            $this->response($employee, 200);
        }
    }

    public function login_post() {
        $email = $this->post('email');
        $password = $this->post('password');

        // Validate user & password
        $user = $this->UserModel->getUserByEmail($email);

        if (!$user || !password_verify($password, $user['password'])) {
            $this->response(['error' => 'Invalid credentials!'], 401);
            return;
        }

        // Set the logged-in flag in the session
        $this->session->set_userdata('loggedIn', true);
        $this->session->set_userdata('userId', $user['id']);

        $this->response(['message' => 'Login successful'], 200);
    }

    public function logout_get() {
        $this->session->sess_destroy();
        $this->response(['message' => 'Logout successful'], 200);
    }

    private function isLoggedIn() {
        return $this->session->userdata('loggedIn') === true;
    }

}
